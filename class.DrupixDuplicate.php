<?

/**
 * Class that represents a duplicate operation
 */
class DrupixDuplicate {
    
  /**
   * The site name
   * @var String
   */    
  private $name;
  
  /**
   * The database name
   * @var String
   */
  private $db;
  
  /**
   * The Drupix settings
   * @var Array
   */
  private $drupix;
  
  /**
   * Creates a new duplicater
   */
  public function __construct($name, $db) {
    
    $this->name = $name;
    
    //Iff $db is not specified, set the default value
    if ($db === NULL) {
      $i = strpos($name, '.');
      if ($i === false) {
        $db = $name;
      } else {
        $db = substr($name, 0, $i);
      }
    }
    
    $this->db = $db;
   
    //Get Drupix config
    $this->drupix = drupix_get_config();
  }
  
  /**
   * Validates this duplicate command, and sets error(s) if the command is incorrect
   */
  public function validate() {
    if (!$this->drupix) {
      return drush_set_error('DRUPIX_CONFIG', dt('Drupix config not found or incorrect.'));
    }
    
    //Check required user
    $requiredUser = $this->drupix['required-user'];
    if ($requiredUser) {
      if (exec("whoami") != $requiredUser) {
        return drush_set_error('DRUPIX_CONFIG', dt('The current user is incorrect. Please log in as !requiredUser', array('!requiredUser' => $requiredUser)));
      }
    }
    
    //Check if symbolic link
    $target = @readlink(DRUPAL_ROOT . "/" . conf_path());
    if ($target) {
      return drush_set_error('SITE_IS_DYNAMIC_LINK', dt('You can\'t run this command on a symbolic link. Please run it on ' . $target));
    }
    
    //Check that the database is a mysql database
    $info = reset(Database::getConnectionInfo());
    if ($info['driver'] != 'mysql') {
      return drush_set_error('NO_MYSQL_DATABASE', dt('This command is only supported for mysql databases, but the database of this site is of type: !driver.', array('!driver' => $info['driver'])));
    }
    
    if (reset($info['prefix']) != '') {
      return drush_set_error('PREFIX_NOT_SUPPORTED', dt('This command is not supported for prefixed table database, but this site\'s database has a prefix: !prefix.', array('!prefix' => reset($info['prefix']))));
    }
        
    //Check inputs
    if ($this->name == NULL) {
      return drush_set_error('NO_SITE_NAME', dt('Please specify the new site name.'));
    } else if (!preg_match("/^[a-z0-9\.]{4,}$/i", $this->name)) {
      return drush_set_error('WRONG_SITE_NAME', dt('The site name is incorrect. It may only consist out of (at least 4) alphanumeric characters or points.'));
    }
    
    //Check database name  
    if (!preg_match("/^[a-z0-9\_]{2,}$/i", $this->db)) {
      return drush_set_error('WRONG_DB_NAME', dt("The database name '{$this->db}' is incorrect. It may only consist out of (at least 2) alphanumeric characters or underscores."));
    }
  
    //Check if directory already exists
    if (file_exists(DRUPAL_ROOT . "/sites/" . $this->name)) {
      return drush_set_error('SITE_ALREADY EXISTS', dt('A site with this name already exists. Delete this one first using the delete command.'));
    }
  }
  
  /**
   * Performs the duplicate action
   * @pre duplicate command was validated correctly
   */
  public function execute() {
    try {
      //Create the database
      require_once("fn.createSiteDatabase.php");
      $tmp = createSiteDatabase($this->drupix, $this->db);

      //Complete database info
      $info = array_merge(reset(Database::getConnectionInfo()), $tmp);

      //Export current site's database
      $curInfo = reset(Database::getConnectionInfo());
      
      //Export current database
      $expCmd = 'mysqldump';
      $expCmd .= ' --host=' . escapeshellarg($curInfo['host']);
      $expCmd .= ' --user=' . escapeshellarg($curInfo['username']);
      $expCmd .= ' --password=' . escapeshellarg($curInfo['password']);
      if ($curInfo['port'] != '') {
        $expCmd .= ' --port=' . escapeshellarg($curInfo['port']);
      }
      $expCmd .= ' ' . escapeshellarg($curInfo['database']);
      
      //Import database
      $impCmd = 'mysql';
      $impCmd .= ' --host=' . escapeshellarg($info['host']);
      $impCmd .= ' --user=' . escapeshellarg($info['username']);
      $impCmd .= ' --password=' . escapeshellarg($info['password']);
      if ($info['port'] != '') {
        $impCmd .= ' --port=' . escapeshellarg($info['port']);
      }
      $impCmd .= ' ' . escapeshellarg($info['database']);
      
      //Chain commands
      $cmd = "$expCmd | $impCmd";
      if (!drush_shell_exec($cmd)) {
        throw new Exception("Can't import database: " . $cmd);
      }
      drush_print("Ex- and imported database to " . $info['database']);
      
      //Copy all site files
      $cmd = "cp -R " . $this->getSiteLoc() . " " . $this->getNewSiteLoc();
      if (!drush_shell_exec($cmd)) {
        throw new Exception("Can't copy site files: " . $cmd);
      }
      
      drush_print("Copied all files from " . $this->getSiteLoc() . " to " . $this->getNewSiteLoc());
      
      //Make sure that the files writable for this user
      $cmd = "chmod -R 0770 " . $this->getNewSiteLoc();
      if (!drush_shell_exec($cmd)) {
        drush_print("Warning: can't set files: " . $cmd);
      }
      
      //Remove settings.php
      $settingsPath = $this->getNewSiteLoc() . "/settings.php";
      if (!drush_shell_exec("rm " . $settingsPath)) {
        drush_print("Warning: can't remove current settings file: " . $settingsPath);
      } else {
        drush_print("Removed current settings file: " . $settingsPath);
      }
     
      //Write a new settings file
      $this->writeSettings($info);

      //Replace previous site name by new site name
      $search = conf_path();
      $replacement = "sites/" . $this->name;
      $cwd = getcwd();
      chdir($this->getNewSiteLoc());
      drush_print("Using the db-replace command to replace the site location in the database: '$search' to '$replacement'");
      drush_shell_exec('drush db-replace ' . escapeshellarg($search) . ' ' . escapeshellarg($replacement));
      
      //Clear the cache
      drush_print("Using the cc command to clear the new site's cache");
      drush_shell_exec('drush cc all');
      chdir($cwd);
      
      //Create link if domain is not supported
      if ($this->drupix['domain']) {
        $domain = "." . $this->drupix['domain'];
        $newSiteLoc = $this->getNewSiteLoc();
        if (strrpos($newSiteLoc, $domain) + strlen($domain) != strlen($newSiteLoc)) {
          //New site location does not end with domain: create link
          $name = array_shift(explode('.', $newSiteLoc));
          $cmd = "ln -s " . $newSiteLoc . " {$name}{$domain}";
          if (!drush_shell_exec($cmd)) {
            drush_print("Warning: can't create a dynamic link: " . $cmd);
          } else {
            drush_print("Created a dynamic link for the website: {$name}{$domain}");
          }
        }
      }
      
      drush_log("The site has been duplicated. You can now visit the duplicated website at http://{$this->name}", 'success');
    } catch(Exception $e) {
      //Return general error
      return drush_set_error('GENERAL_ERROR', "" . $e->getMessage());
    }
  }

  /**
   * Returns the base directory of the current site
   */
  private function getSiteLoc() {
    return DRUPAL_ROOT . "/" . conf_path();
  }
  
  /**
   * Returns the base directory of the new site
   */
  private function getNewSiteLoc() {
    return DRUPAL_ROOT . "/sites/" . $this->name;
  }

  /**
   * Writes the database info to a new settings file
   */
  private function writeSettings($info) {
    require_once("fn.getSettingsFileContent.php");
    $content = getSettingsFileContent($info);

    //Write to settings.php
    $settingsPath = $this->getNewSiteLoc() . "/settings.php";
    if (file_put_contents($settingsPath, $content) === FALSE) {
      throw new Exception("Can't write settings file to '{$settingsPath}'");
    }
     
    drush_print("Written settings file: " . $settingsPath);
    
    //Set not writable and readable for all
    $cmd = "chmod 0400 " . $settingsPath;
    if (!drush_shell_exec($cmd)) {
      drush_print("Warning: can't set files to writable in '" . $this->getNewSiteLoc() . "'");
    }
  }

}

