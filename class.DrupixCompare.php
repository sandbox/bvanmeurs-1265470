<?

require_once("class.DrupixRemote.php");

/**
 * Class that represents a compare operation
 */
class DrupixCompare extends DrupixRemote {
    
  /**
   * Creates a new duplicater
   */
  public function __construct($multisite) {
    parent::__construct($multisite);
  }

  /**
   * Performs the compare action
   * @pre duplicate command was validated correctly
   */
  public function execute() {
    try {
      include "fn.getSystem.php";
      
      //Get local multisite info
      $cwd = getcwd();
      chdir(DRUPAL_ROOT);
      $local = getSystem();
      chdir($cwd);
      
      //Get remote multisite info
      $remote = $this->getRemoteSystemInfo();
      
      //Compare local sys to remote sys
      $results = $this->compare($local, $remote);
      
      if (count($results) === 0) {
        drush_log("Local multisite is compatible with {$this->multisite}", 'success');
      } else {
        $str = sprintf("| %6s | %25s | %15s | %15s |", 
          "type", "name", "local", $this->multisite);
          drush_print($str);
          
        foreach ($results as $r) {
          $str = sprintf("| %6s | %25s | %15s | %15s |", 
            $r[0], $r[1], $r[2], $r[3]);
          drush_print($str);
        }
        
        drush_log("Local multisite is NOT compatible with {$this->multisite}!", 'warning');
      }
      
    } catch(Exception $e) {
      //Return general error
      return drush_set_error('GENERAL_ERROR', "" . $e->getMessage());
    }
  }

  /**
   * Compares the local system to the remote multisite
   * @return String[]   All actions that should be performed to make the multisites compatible
   */
  private function compare($local, $remote) {
    $results = array();
    
    //Check Drupal core
    if ($local['core'] != $remote['core']) {
      $results[] = array('Drupal', 'core', $local['core'], $remote['core']);      
    }
    
    //Add modules and themes that exist on local but not on remote
    $names = array_diff(array_keys($local['modules']), array_keys($remote['modules']));
    foreach ($names as $name) {
    var_dump($local['modules'][$name]);
      $results[] = array('Module', $name, $local['modules'][$name]->info['version'], 'Not installed');
    }

    $names = array_diff(array_keys($local['themes']), array_keys($remote['themes']));
    foreach ($names as $name) {
      $results[] = array('Theme', $name, $local['themes'][$name]->info['version'], 'Not installed');
    }

    //Add modules and themes that exist on remote but not on local
    $names = array_diff(array_keys($remote['modules']), array_keys($local['modules']));
    foreach ($names as $name) {
      $results[] = array('Module', $name, 'Not installed', $remote['modules'][$name]->info['version']);
    }
    
    $names = array_diff(array_keys($remote['themes']), array_keys($local['themes']));
    foreach ($names as $name) {
      $results[] = array('Theme', $name, 'Not installed', $remote['modules'][$name]->info['version']);
    }
    
    //Check module versions
    $modules = array_intersect(array_keys($local['modules']), array_keys($remote['modules']));
    foreach ($modules as $name) {
      $localVersion = $local['modules'][$name]->info['version'];
      $remoteVersion = $remote['modules'][$name]->info['version'];
      if ($localVersion != $remoteVersion) {
        $results[] = array('Module', $name, $localVersion, $remoteVersion);
      }
    }
    
    //Ceck theme versions
    $themes = array_intersect(array_keys($local['themes']), array_keys($remote['themes']));
    foreach ($themes as $name) {
      $localVersion = $local['themes'][$name]->info['version'];
      $remoteVersion = $remote['themes'][$name]->info['version'];
      if ($localVersion != $remoteVersion) {
        $results[] = array('Module', $name, $localVersion, $remoteVersion);
      }
    }   
    return $results;
  }

  /**
   * Returns the system info of the remote system
   */
  private function getRemoteSystemInfo() {
    //Get remote configuration
    $rConfig = $this->getRemoteMultisiteConfig();
    $config = $this->getMultisiteConfig();
    
    if ($config['www'] . "\n" != $this->remoteCommand('pwd')) {
      throw new Exception("Communication problem with remote multisite.");
    }
    
    //Create PHP for requesting the system listing
    $contents = file_get_contents(DRUSH_BASE_PATH . "/commands/drupix/fn.getSystem.php");
    $copySvs = array('HTTP_HOST', 'PHP_SELF', 'SCRIPT_NAME', 'REQUEST_URI', 'REMOTE_ADDR', 'REQUEST_METHOD', 'SERVER_SOFTWARE', 'HTTP_USER_AGENT');
    $serverVars = array();
    foreach ($copySvs as $c) {
      $serverVars[$c] = $_SERVER[$c];
    }
    $contents .= "\n\nchdir('{$config['www']}');\n\$serverVars = " . var_export($serverVars, TRUE) . ";\n\nbootstrap(\$serverVars);\necho serialize(getSystem());";
    $filename = "compare." . rand() . ".php";
    $path = "{$this->drupix['temp']}/{$filename}";
    if (file_put_contents($path, $contents) === false) {
      throw new Exception("Can't save temporary file: {$path}");
    }
    
    //Get system listing
    $out = $this->remoteCommand("php", NULL, $path);
    
    //Delete temporary file   
    unlink($path);
    
    //Unserialize into $temp
    $temp = unserialize($out);
    if ($temp === false) {
      throw new Exception("The received system array is not serialized correctly");
    }
    
    return $temp;
  }
  
}