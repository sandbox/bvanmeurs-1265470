<?

/**
 * Class that represents a remove operation
 */
class DrupixRemove {
    
  /**
   * The Drupix settings
   * @var Array
   */
  private $drupix;
  
  /**
   * Creates a new remove
   */
  public function __construct() {
     //Get Drupix config
    $this->drupix = drupix_get_config();
  }
  
  /**
   * Validates this duplicate command, and sets error(s) if the command is incorrect
   */
  public function validate() {
    if (!$this->drupix) {
      return drush_set_error('DRUPIX_CONFIG', dt('Drupix config not found or incorrect.'));
    }
    
    //Check required user
    $requiredUser = $this->drupix['required-user'];
    if ($requiredUser) {
      if (exec("whoami") != $requiredUser) {
        return drush_set_error('DRUPIX_CONFIG', dt('The current user is incorrect. Please log in as !requiredUser', array('!requiredUser' => $requiredUser)));
      }
    }    
    
    //Check if symbolic link
    $target = @readlink(DRUPAL_ROOT . "/" . conf_path());
    if ($target) {
      return drush_set_error('SITE_IS_DYNAMIC_LINK', dt('You can\'t run this command on a symbolic link. Please run it on ' . $target));
    }
    
    //Check that the database is a mysql database
    $info = reset(Database::getConnectionInfo());
    if ($info['driver'] != 'mysql') {
      return drush_set_error('NO_MYSQL_DATABASE', dt('This command is only supported for mysql databases, but the database of this site is of type: !driver.', array('!driver' => $info['driver'])));
    }
    if (reset($info['prefix']) != '') {
      return drush_set_error('PREFIX_NOT_SUPPORTED', dt('This command is not supported for prefixed table database, but this site\'s database has a prefix: !prefix.', array('!prefix' => reset($info['prefix']))));
    }
  }
  
  /**
   * Performs the duplicate action
   * @pre duplicate command was validated correctly
   */
  public function execute() {
    try {
      //Get db name and user name
      $info = reset(Database::getConnectionInfo());
      $dbName = $info['database'];
      $userName = $info['username'];

      if (!drush_confirm("Are you sure you want to remove the database {$dbName}, the database user {$userName} and the site directory " . $this->getSiteLoc() . "?")) {
        drush_print("Deletion aborted.");
        return TRUE;
      }
      
      //Back up current site
      drush_shell_cd_and_exec($this->getSiteLoc(), "drush backup");
      drush_print("The site has been backed up.");
      
      //Delete symbolic links
      if ($handle = opendir(DRUPAL_ROOT . "/sites")) {
        while (false !== ($file = readdir($handle))) {
          $path = DRUPAL_ROOT . "/sites/" . $file;
          $target = @readlink($path);
          if ($target == $this->getSiteLoc()) {
            unlink($path);
            drush_print("Deleted symbolic link: " . $path);
          }
        }
        closedir($handle);
      }
        
      //Connect to database
      $this->conn = @mysql_connect($this->drupix['db-host'], $this->drupix['db-user'], $this->drupix['db-pass']);
      if (!$this->conn) return drush_set_error('DB_PROBLEM', dt('Can\'t connect to database.'));
      
      //Remove the database
      if (!@mysql_query("DROP DATABASE `{$dbName}`", $this->conn)) {
        drush_print("Warning: can't remove database '{$dbName}: " . mysql_error($this->conn));
      } else {
        drush_print("Removed database '{$dbName}'");
      }

      //Remove the user
      if (!@mysql_query("DROP USER '{$userName}'", $this->conn)) {
        drush_print("Warning: can't remove user '{$userName}': " . mysql_error($this->conn));
      } else {
        drush_print("Removed db user '{$userName}'");
      }
      
      //Remove all site files
      $cmd = "chmod -R u+w " . $this->getSiteLoc();
      if (!drush_shell_exec($cmd)) {
        drush_print("Warning: can't set files to writable in '" . $this->getSiteLoc() . "'");
      }
      
      $cmd = "rm -f -R " . $this->getSiteLoc();
      if (!drush_shell_exec($cmd)) {
        drush_print("Warning: can't remove site files in '" . $this->getSiteLoc() . "'");
      } else {
        drush_print("Removed complete directory " . $this->getSiteLoc());
      }
      
      drush_log("The site has been removed.", 'success');

      drush_die("Drush will now exit. Please note that you may now be in a non-existing directory. Please ignore the following error!", 0);
    } catch(Exception $e) {
      //Return general error
      return drush_set_error('GENERAL_ERROR', "" . $e->getMessage());
    }
  }

  /**
   * Returns the base directory of the current site
   */
  private function getSiteLoc() {
    return DRUPAL_ROOT . "/" . conf_path();
  }

}