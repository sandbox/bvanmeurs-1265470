<?php

/**
 * Writes the database info to a new settings file
 * @param Array $info   Drupal Database info array
 */
function getSettingsFileContent($info) {
  $content = '<?php

//Import global settings
include("' . DRUPAL_ROOT . '/sites/global_settings.php");

$databases = array (
  "default" => array (
    "default" => array (
      "database" => "' . $info["database"] . '",
    "username" => "' . $info["username"] . '",
    "password" => "' . $info["password"] . '",
    "host" => "' . $info["host"] . '",
    "port" => "' . $info["port"] . '",
      "driver" => "mysql",
      "prefix" => "",
    ),
  ),
);';

  return $content;
}