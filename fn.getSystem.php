<?php

/**
 * Bootstraps Drupal; this must be done before calling getSystem if Drupal hasn't already been bootstrapped
 * @param Array $serverVars   The server vars to be used (to specify the correct site) 
 * @pre current directory is the Drupal root
 */
function bootstrap($serverVars) {
  define('DRUPAL_ROOT', getcwd());
  $_SERVER = array_merge($_SERVER, $serverVars);
  require_once './includes/common.inc';
  require_once './includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
}

/**
 * Returns a an array with information of the core, modules and themes in this Drupal installation
 */
function getSystem() {
  $system = array();
  $system['core'] = VERSION;
  $system['modules'] = system_rebuild_module_data();
  $system['themes'] = system_rebuild_theme_data();
  return $system;
}
