<?

require_once("class.DrupixRemote.php");

/**
 * Class that represents a migrate operation
 */
class DrupixMigrate extends DrupixRemote {
    
  /**
   * Creates a new migrater
   */
  public function __construct($multisite) {
    parent::__construct($multisite);
  }

  /**
   * Validates this command, and sets error(s) if the command is incorrect
   */
  public function validate() {
    $retVal = parent::validate();
    if ($retVal !== NULL) return $retVal;
    
    //Check if symbolic link
    $target = @readlink(DRUPAL_ROOT . "/" . conf_path());
    if ($target) {
      return drush_set_error('SITE_IS_DYNAMIC_LINK', dt('You can\'t run this command on a symbolic link. Please run it on ' . $target));
    }
    
    //Check that the database is a mysql database
    $info = reset(Database::getConnectionInfo());
    if ($info['driver'] != 'mysql') {
      return drush_set_error('NO_MYSQL_DATABASE', dt('This command is only supported for mysql databases, but the database of this site is of type: !driver.', array('!driver' => $info['driver'])));
    }
    if (reset($info['prefix']) != '') {
      return drush_set_error('PREFIX_NOT_SUPPORTED', dt('This command is not supported for prefixed table database, but this site\'s database has a prefix: !prefix.', array('!prefix' => reset($info['prefix']))));
    }
    
    //Check if the multisites are compatible
    if (!drush_shell_cd_and_exec(DRUPAL_ROOT, 'drush compare ' . escapeshellarg($this->multisite))) {
      return drush_set_error('COMPARE_ERROR', dt("Can't perform the Drupix compare command (drush compare) on the default site"));
    }
    $lines = drush_shell_exec_output();
    
    $correct = false;
    $search = 'Local multisite is compatible with ' . $this->multisite;
    foreach ($lines as $line) {
      if (strpos($line, $search) !== false) {
        $correct = true;
        break;
      }
    }
    
    if (!$correct) {
      return drush_set_error('COMPARE_ERROR', dt("This multisite in incompatible with !multisite. Please run the compare-command and make them compatible.", array('!multisite' => $this->multisite)));      
    }
  }
  
  /**
   * Performs the compare action
   * @pre duplicate command was validated correctly
   */
  public function execute() {
    try {
      //Check if site exists
      $config = $this->getMultisiteConfig();
      $rConfig = $this->getRemoteMultisiteConfig();
      $remSiteDir = $config['www'] . '/' . conf_path();
      
      //Check if site directory exists
      $exists = true;
      try {
        $this->remoteCommand('ls', $remSiteDir);
      } catch(Exception $e) {
       $exists = false;
      }
      if ($exists) {
        throw new Exception('Site already exists on remote multisite, please remove it first: ' . $remSiteDir);
      }
      
      //Get local db info
      $info = reset(Database::getConnectionInfo());
      
      require_once("fn.createSiteDatabase.php");
      
      //Get db name without prefix and postfix
      $db = $info['database'];
      if (strpos($db, $this->drupix['db-prefix']) === 0) {
        //Strip db prefix
        $db = substr($db, strlen($this->drupix['db-prefix']));
      }
      if ($matches = preg_match("/^(.*)_\d+$/", $db)) {
        $db = $matches[1];
      }
      
      //Get remote db info
      $dbInfo = createSiteDatabase($rConfig, $db, $config['host']);
      
      //Export current database and import into new database
      $expCmd = 'mysqldump';
      $expCmd .= ' --host=' . escapeshellarg($info['host']);
      $expCmd .= ' --user=' . escapeshellarg($info['username']);
      $expCmd .= ' --password=' . escapeshellarg($info['password']);
      if ($info['port'] != '') {
        $expCmd .= ' --port=' . escapeshellarg($info['port']);
      }
      $expCmd .= ' ' . escapeshellarg($info['database']);
      
      //Import database
      $impCmd = 'mysql';
      $impCmd .= ' --host=' . escapeshellarg($config['host']);
      $impCmd .= ' --user=' . escapeshellarg($dbInfo['username']);
      $impCmd .= ' --password=' . escapeshellarg($dbInfo['password']);
      if ($dbInfo['port'] != '') {
        $impCmd .= ' --port=' . escapeshellarg($dbInfo['port']);
      }
      $impCmd .= ' ' . escapeshellarg($dbInfo['database']);
      
      //Chain commands
      $cmd = "$expCmd | $impCmd";
      if (!drush_shell_exec($cmd)) {
        throw new Exception("Can't import database: " . $cmd);
      }
      drush_print("Ex- and imported database to " . $dbInfo['database']);
      
      //Copy all files to remote multisite
      $siteLoc = $this->getSiteLoc();
      $cmd = "rsync -vaz --exclude=settings.php -e ssh ./ {$config['ssh-user']}@{$config['host']}:{$remSiteDir}";
      if (!drush_shell_cd_and_exec($siteLoc, $cmd)) throw new Exception("Can't rsync: " . $cmd);
      
      drush_print("Copied all files from local multisite to remote multisite at " . $remSiteDir);
      
      //Set rights 
      $this->remoteCommand("chmod -R 0770 {$remSiteDir}");
      
      //Write settings file
      require_once("fn.getSettingsFileContent.php");
      $content = getSettingsFileContent($dbInfo);
  
      //Write to settings.php
      $filename = "settings." . rand() . ".php";
      $path = "{$this->drupix['temp']}/{$filename}";
      if (file_put_contents($path, $content) === false) {
        throw new Exception("Can't save temporary file: {$path}");
      }
      $cmd = "rsync -vaz -e ssh {$filename} {$config['ssh-user']}@{$config['host']}:{$remSiteDir}/settings.php";
      if (!drush_shell_cd_and_exec($this->drupix['temp'], $cmd)) {
        unlink($path);
        throw new Exception("Can't rsync settings.php: " . $cmd);
      }
      drush_print("Written settings file on remote multisite: {$remSiteDir}/settings.php");
            
      unlink($path);
      
      //Create link if domain is not supported
      if ($rConfig['domain']) {
        $domain = "." . $rConfig['domain'];
        if (strrpos($remSiteDir, $domain) + strlen($domain) != strlen($remSiteDir)) {
          //New site location does not end with domain: create link
          $name = array_shift(explode('.', $remSiteDir));
          $cmd = "ln -s " . $remSiteDir . " {$name}{$domain}";
          try {
            $this->remoteCommand($cmd, $config['www'] . "/sites");
          } catch (Exception $e) {
            drush_print("Warning: can't create a dynamic link: " . $cmd);
          }
          drush_print("Created a dynamic link for the website: {$name}{$domain}");
        }
      }
      
      //Try to clear cache using local Drush
      try {
        $this->remoteCommand('drush cc all', $remSiteDir);
        drush_print("The cache of the migrated site has been cleared");
      } catch(Exception $e) {
        drush_print("The cache of the migrated site could not be cleared. Is Drush installed?");
      }
      
      drush_log("The site has been migrated. You can now visit the duplicated website on the multisite named '{$this->multisite}'", 'success');
    } catch(Exception $e) {
      //Return general error
      return drush_set_error('GENERAL_ERROR', "" . $e->getMessage());
    }
  }

  /**
   * Returns the base directory of the current site
   */
  private function getSiteLoc() {
    return DRUPAL_ROOT . "/" . conf_path();
  }

}