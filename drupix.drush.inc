<?php

/**
 * @file Drush Drupix commands
 */

/**
 * Implementation of hook_drush_help().
 */
function drupix_drush_help($section) {
  switch ($section) {
    case 'meta:drupix:title':
      return dt('Drupix tools for Drush');
    case 'meta:drupix:summary':
      return dt('Helps you with tools to manage a Drupal site, thanks to Drupix internet!');
    case 'drush:db-replace':
      return dt("Replaces the specified term with another term in the Drupal database of the specified site. Also checks (recursively) serialized data");
    case 'drush:duplicate':
      return dt("Duplicates an existing Drupal site to another one (within the same multisite)");
    case 'drush:remove':
      return dt("Removes the specified site. Site is backed up before deletion");
    case 'drush:backup':
      return dt("Creates a backup of the specified site");
    case 'drush:compare':
      return dt("Compares the multisite that the specified site is in to the specified other multisite. The 'default' site is always compared, regardless of what site is specified");
    case 'drush:migrate':
      return dt("Migrates the specified site to the specified multisite");
  }
}

/**
 * Implementation of hook_drush_command().
 */
function drupix_drush_command() {
  $items['db-replace'] = array(
    'description' => 'Replaces the specified term with another term in the Drupal database of the specified site. Also checks (recursively) serialized data',
    'arguments' => array(
      'search' => 'The term to be replaced',
      'replacement' => 'The replacement',
    ),
    'examples' => array(
      'db-replace "sites/default" "sites/mynewdomain.com"' => 'Replaces all occurrences for sites/default with sites/mynewdomain.com',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(),
  );
  $items['duplicate'] = array(
    'description' => 'Duplicates an existing Drupal site to another one (within the same multisite)',
    'arguments' => array(
      'name' => 'Name of the new website',
      'db' => 'Name of the database of the website',
    ),
    'examples' => array(
      'duplicate "mynewdomain.com"' => 'Duplicates to current site to mynewdomain.com',
      'duplicate "mynewdomain.com" "mydbname"' => 'Duplicates to current site to mynewdomain.com, with database user and db named "mydbname"',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(),
  );
  $items['remove'] = array(
    'description' => 'Removes the specified site',
    'arguments' => array(
    ),
    'examples' => array(
      'remove' => 'Removes the current site',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(),
  );
  $items['backup'] = array(
    'description' => 'Creates a backup of the specified site',
    'arguments' => array(
    ),
    'examples' => array(
      'backup' => 'Creates a backup of the specified site',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array('no-core' => 'Iff specified, then the core code and sites/all will not be backed up. This takes less storage space, but when updating the orignating site this backup may become invalid'),
  );
  $items['compare'] = array(
    'description' => 'Compares this multisite with the other multisite',
    'arguments' => array(
      'multisite' => 'Name of the multisite'
    ),
    'examples' => array(
      'compare live' => 'Compares this multisite with the live multisite',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(),
  );
  $items['migrate'] = array(
    'description' => 'Migrates the specified site to the specified multisite',
    'arguments' => array(
      'multisite' => 'Name of the multisite'
    ),
    'examples' => array(
      'migrate live' => 'Migrates this site to the (remote) multisite named \'live\'',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(),
  );
  
  return $items;
}

/**
 * Returns the Drupix configuration, or FALSE if it doesn't exist or is incorrect
 */
function drupix_get_config() {
  $path = DRUPAL_ROOT . "/drupixrc.php";
  if (!is_file($path)) {
    return FALSE;
  }
  
  include($path);
  if (!isset($drupixConfig)) return FALSE;
  
  return $drupixConfig;
}

/**
 * Backup the specified site
 */

function drush_drupix_backup_validate() {
  require_once("class.DrupixBackup.php");
  $dcc = new DrupixBackup();
  return $dcc->validate();
}
   
function drush_drupix_backup() {
  require_once("class.DrupixBackup.php");
  $dcc = new DrupixBackup();
  return $dcc->execute();
}

/**
 * Replace a term in the database of the specified site
 */

function drush_drupix_db_replace_validate($search = NULL, $replacement = NULL) {
  require_once("class.DrupixDbReplace.php");
  $replacer = new DrupixDbReplace($search, $replacement);
  return $replacer->validate();
}

function drush_drupix_db_replace($search, $replacement) {
  require_once("class.DrupixDbReplace.php");
  $replacer = new DrupixDbReplace($search, $replacement);
  return $replacer->execute();
}

/**
 * Duplicate the specified site
 */

function drush_drupix_duplicate_validate($name = NULL, $db = NULL) {
  require_once("class.DrupixDuplicate.php");
  $dcc = new DrupixDuplicate($name, $db);
  return $dcc->validate();
}
   
function drush_drupix_duplicate($name = NULL, $db = NULL) {
  require_once("class.DrupixDuplicate.php");
  $dcc = new DrupixDuplicate($name, $db);
  return $dcc->execute();
}

/**
 * Remove the specified site
 */
function drush_drupix_remove_validate() {
  require_once("class.DrupixRemove.php");
  $dcc = new DrupixRemove();
  return $dcc->validate();
}
   
function drush_drupix_remove() {
  require_once("class.DrupixRemove.php");
  $dcc = new DrupixRemove();
  return $dcc->execute();
}


/**
 * Compare this multisite to another multisite
 */
function drush_drupix_compare_validate($multisite = NULL) {
  require_once("class.DrupixCompare.php");
  $dcc = new DrupixCompare($multisite);
  return $dcc->validate();
}
   
function drush_drupix_compare($multisite = NULL) {
  require_once("class.DrupixCompare.php");
  $dcc = new DrupixCompare($multisite);
  return $dcc->execute();
}

/**
 * Migrates this site to another multisite
 */
function drush_drupix_migrate_validate($multisite = NULL) {
  require_once("class.DrupixMigrate.php");
  $dcc = new DrupixMigrate($multisite);
  return $dcc->validate();
}
   
function drush_drupix_migrate($multisite = NULL) {
  require_once("class.DrupixMigrate.php");
  $dcc = new DrupixMigrate($multisite);
  return $dcc->execute();
}