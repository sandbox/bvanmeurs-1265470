<?php

/**
 * Creates a database for the new site, and returns the database information
 * @param $drupix Drupix configuration of destination server
 * @param $db     Database base name for the new site
 * @param $host   Alternative db host than specified in the $drupix file
 * @return [database => String, username => String, password => String]
 */
function createSiteDatabase($drupix, $db, $host = NULL) {
  if ($host === NULL) $host = $drupix['db-host'];
  $conn = @mysql_connect($host, $drupix['db-user'], $drupix['db-pass']);
  if (!$conn) throw new Exception(dt('Can\'t connect to database.'));
  
  $postfix = 0;
  $dbName = NULL;
  $dbPass = rand();
  while(true) {
    $dbName = $drupix['db-prefix'] . $db;
    if ($postfix > 0) $dbName = substr($dbName, 0, 16 - strlen("_" . $postfix)) . "_" . $postfix;
    
    $result = mysql_query("SELECT IF(EXISTS(SELECT * FROM information_schema.schemata WHERE schema_name = '{$dbName}') OR EXISTS(SELECT * FROM mysql.user WHERE user = '{$dbName}'), 1, 0) AS e", $conn);
    if ($result === FALSE) {
      throw new Exception("Error while executing query: " . mysql_error($conn));
    }
    $record = mysql_fetch_array($result);
    mysql_free_result($result);
    if ($record['e'] == "1") {
      //Database name is in use: try next postfix value
      $postfix++;
    } else {
      break;
    }
  }

  if (!@mysql_query("CREATE USER '{$dbName}'@'%' IDENTIFIED BY '{$dbPass}'", $conn)) {
    throw new Exception("Can't create user '{$dbName}': " . mysql_error($conn));
  }
  drush_print("Created db user '{$dbName}' with password '{$dbPass}'");
  
  if (!@mysql_query("CREATE DATABASE `{$dbName}` CHARSET utf8", $conn)) {
    throw new Exception("Can't create database '{$dbName}': " . mysql_error($conn));
  }
  drush_print("Created database '{$dbName}'");

  if (!@mysql_query("GRANT ALL ON `{$dbName}`.* TO {$dbName}@'%'", $conn)) {
    throw new Exception("Can't grant ALL rights on database '$dbName' to user '{$dbName}': " . mysql_error($conn));
  }
  drush_print("Grant rights for database '{$dbName}' to user '{$dbName}'");
  
  return array(
    'database' => $dbName,
    'username' => $dbName, 
    'password' => $dbPass,
  );
  
  mysql_close($conn);
}
