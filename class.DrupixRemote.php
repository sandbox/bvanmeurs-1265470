<?

/**
 * Class that represents a remote operation
 */
class DrupixRemote {
    
  /**
   * The Drupix settings
   * @var Array
   */
  protected $drupix;
  
  /**
   * The name of the (remote) multisite to be compared
   */
  protected $multisite;
  
  /**
   * The remote multisite config
   */
  private $remoteMultisiteConfig;
  
  /**
   * Creates a new remote command
   * @param String $multisite   The name of the remote multisite
   */
  public function __construct($multisite) {
    $this->multisite = $multisite;

     //Get Drupix config
    $this->drupix = drupix_get_config();
    
    $this->remoteMultisiteConfig = NULL;
  }

  /**
   * Returns the multisite configuration, or FALSE if it doesn't exist
   */  
  protected function getMultisiteConfig() {
    if (!is_array($this->drupix['multisite'])) return FALSE;
    $config = $this->drupix['multisite'][$this->multisite];
    if (!is_array($config)) return FALSE;
    return $config;
  }

  /**
   * Returns the extended multisite configuration: the multisite config with the remote config added to it
   */
  protected function getRemoteMultisiteConfig() {
    if ($this->remoteMultisiteConfig === NULL) {
      $config = $this->getMultisiteConfig();
      
      //Get the drupix configuration file
      $configPhp = $this->remoteCommand("cat {$config['www']}/drupixrc.php");
      if (!$configPhp) throw new Exception("drupixConfig not set in drupixrc.php on remote multisite");

      //Load configuration
      unset($drupixConfig);
      eval("?>" . $configPhp);
      if (!isset($drupixConfig)) throw new Exception("drupixConfig not set in drupixrc.php on remote multisite");
      $this->remoteMultisiteConfig = $drupixConfig;
    }
    return $this->remoteMultisiteConfig;
  }
  
  /**
   * Validates this duplicate command, and sets error(s) if the command is incorrect
   */
  public function validate() {
    if (!$this->drupix) {
      return drush_set_error('DRUPIX_CONFIG', dt('Drupix config not found or incorrect.'));
    }
    
    //Check required user
    $requiredUser = $this->drupix['required-user'];
    if ($requiredUser) {
      if (exec("whoami") != $requiredUser) {
        return drush_set_error('DRUPIX_CONFIG', dt('The current user is incorrect. Please log in as !requiredUser', array('!requiredUser' => $requiredUser)));
      }
    }    
    
    //Check compares directory
    if ($this->multisite === NULL) {
      return drush_set_error('NO_MULTISITE_NAME', dt('Please specify the multisite to compare to'));
    } else if (!$this->getMultisiteConfig()) {
      return drush_set_error('DRUPIX_CONFIG', dt('Drupix multisite installation not found: !multisite', array('!multisite' => $this->multisite)));
    }
  }
  
  /**
   * Issues the specified command at the remote host (via ssh)
   * @param Array $cmd   The command(s)
   * @param String $cwd   Iff specified, the (absolute) local working directory; defaults to the www directory
   * @param String $input  Iff specified, the local file to be used as input
   * @return String   The command's output
   * @throws Exception iff ssh error occurs (for command error, check return value)
   */
  protected function remoteCommand($cmd, $cwd = NULL, $input = NULL) {
    //Get configuration (local and remote) of remote multisite    
    $config = $this->getMultisiteConfig();
    
    if ($cwd === NULL) $cwd = $config['www'];
      
    if (is_array($cmd)) {
      //Implode command
      $cmd = implode(' && ', $cmd);
    }

    //Build complete command
    $ccmd = "cd " . $cwd . " && " . $cmd;
    
    //Build command
    $remCmd = '';
    if ($input !== NULL) {
      $remCmd .= "cat {$input} | ";
    }
    $remCmd .= "ssh " . $config['ssh-user'] . "@" . $config['host'];
    $remCmd .= ' "' . $ccmd . '"';
    $remCmd .= ' 2>&1';
    
    //Execute command
    $res = popen($remCmd, "r");
    if ($res === FALSE) {
      throw new Exception("Error while executing: " . $remCmd);
    }
    
    //Read output
    stream_set_blocking($res, true);
    $out = stream_get_contents($res);
    
    pclose($res);
    if (strpos($out, "sh: line 0:") === 0) {
      //Error!
      throw new Exception("Error while executing: '" . substr($out, 12) . "'");
    } 
    
    //Return output
    return $out;
  }

}