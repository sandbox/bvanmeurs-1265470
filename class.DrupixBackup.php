<?

/**
 * Class that represents a backup operation
 */
class DrupixBackup {
    
  /**
   * The Drupix settings
   * @var Array
   */
  private $drupix;
  
  /**
   * Creates a new duplicater
   */
  public function __construct() {
     //Get Drupix config
    $this->drupix = drupix_get_config();
  }

  /**
   * Returns the backups directory, or FALSE if it doesn't exist
   */  
  private function getBackupsDir() {
    $cwd = getcwd();
    chdir(DRUPAL_ROOT);
    if (!chdir($this->drupix['backup'])) {
      return FALSE;
    } else {
      return getcwd();
    }
    chdir($cwd);
  }
  
  /**
   * Validates this duplicate command, and sets error(s) if the command is incorrect
   */
  public function validate() {
    if (!$this->drupix) {
      return drush_set_error('DRUPIX_CONFIG', dt('Drupix config not found or incorrect.'));
    }
    
    //Check required user
    $requiredUser = $this->drupix['required-user'];
    if ($requiredUser) {
      if (exec("whoami") != $requiredUser) {
        return drush_set_error('DRUPIX_CONFIG', dt('The current user is incorrect. Please log in as !requiredUser', array('!requiredUser' => $requiredUser)));
      }
    }
        
    //Check backups directory
    if (!$this->getBackupsDir()) {
      return drush_set_error('DRUPIX_CONFIG', dt('Drupix backups directory is incorrect: !backups', array('!backups' => $this->drupix['backups'])));
    }

    //Check that the database is a mysql database
    $info = reset(Database::getConnectionInfo());
    if ($info['driver'] != 'mysql') {
      return drush_set_error('NO_MYSQL_DATABASE', dt('This command is only supported for mysql databases, but the database of this site is of type: !driver.', array('!driver' => $info['driver'])));
    }
    if (reset($info['prefix']) != '') {
      return drush_set_error('PREFIX_NOT_SUPPORTED', dt('This command is not supported for prefixed table database, but this site\'s database has a prefix: !prefix.', array('!prefix' => reset($info['prefix']))));
    }
  }
  
  /**
   * Performs the duplicate action
   * @pre duplicate command was validated correctly
   */
  public function execute() {
    try {
      //Create backup directory
      $backupName = array_pop(explode('/', conf_path())) . "-" . date("Ymd_Hi");
      drush_print("Backup name: " . $backupName);

      $backupPath = $this->getBackupsDir() . "/" . $backupName;
      
      if (is_dir($backupPath)) {
        //Remove current directory
        drush_shell_exec("chmod -R ug+rw " . escapeshellarg($backupPath));
        drush_shell_exec('rm -R ' . $backupPath);
      }
      
      if (!mkdir($backupPath, 0770)) {
        throw new Exception("Couldn't create backup directory: " . $backupPath);
      }
      
     //Export current database
      $info = reset(Database::getConnectionInfo());
      $exportPath = $backupPath . '/' . $info['database'] . '.sql';
      $cmd = 'mysqldump';
      $cmd .= ' --host=' . escapeshellarg($info['host']);
      $cmd .= ' --user=' . escapeshellarg($info['username']);
      $cmd .= ' --password=' . escapeshellarg($info['password']);
      if ($curInfo['port'] != '') {
        $cmd .= ' --port=' . escapeshellarg($info['port']);
      }
      $cmd .= ' ' . escapeshellarg($info['database']);
      $cmd .= ' > ' . $exportPath;
      if (!drush_shell_exec($cmd)) {
        throw new Exception("Can't export database: " . $cmd);
      }
      drush_print("Exported database '" . $info['database'] . "' to file: " . $exportPath);
      drush_shell_exec('gzip ' . $exportPath);

      if (!drush_get_option('no-core')) {      //Rsync Drupal core
        $cmd = 'rsync -vaz --exclude=sites ' . escapeshellarg(DRUPAL_ROOT) . ' ' . escapeshellarg($backupPath);
        if (!drush_shell_exec($cmd)) throw new Exception("Can't rsync: " . $cmd);
        
        //Rsync sites/all
        $cmd = 'rsync -vaz ' . escapeshellarg(DRUPAL_ROOT . '/sites/all') . ' ' . escapeshellarg($backupPath . '/www/sites');
        if (!drush_shell_exec($cmd)) throw new Exception("Can't rsync: " . $cmd);
      } else {
        mkdir($backupPath . "/www");
        mkdir($backupPath . "/www/sites");
      }
      
      //Rsync site itself
      $cmd = 'rsync -vaz ' . escapeshellarg(DRUPAL_ROOT . '/' . conf_path()) . ' ' . escapeshellarg($backupPath . '/www/sites');
      if (!drush_shell_exec($cmd)) throw new Exception("Can't rsync: " . $cmd);
      
      //Make archive of directory
      $cmd = 'tar -pczf ' . escapeshellarg('www.tar.gz') . ' ' . escapeshellarg('www');
      if (drush_shell_cd_and_exec($backupPath, $cmd)) {
        //Remove tar-ed files
        drush_shell_exec("chmod -R ug+rw " . escapeshellarg($backupPath . '/www'));
        system("rm -R " . escapeshellarg($backupPath . '/www'));
      }
      
      drush_log("The site has been backed up to {$backupPath}", 'success');
    } catch(Exception $e) {
      //Return general error
      return drush_set_error('GENERAL_ERROR', "" . $e->getMessage());
    }
  }

}