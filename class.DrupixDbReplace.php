<?

/**
 * Class that represents a replacement operation
 */
class DrupixDbReplace {
  
  /**
   * Database connection
   */
  private $conn;
  
  /**
   * Database name
   */
  private $dbName;
  
  /**
   * Table prefix
   */
  private $prefix;
  
  /**
   * The text to be replaced
   */
  public $search;

  /**
   * The replacement
   */
  public $replacement;
  
  /**
   * Creates a new replacer
   */
  public function __construct($search, $replacement) {
    //Set arguments
    $this->search = $search;
    $this->replacement = $replacement;
    
    //Get database connection
    $this->conn = Database::getConnection();

    //Get connection info
    $info = $this->conn->getConnectionOptions();
    
    //Get the database name
    $this->dbName = $info['database'];
    
    //Get the table prefix (we assume that it is used by all tables)
    $this->prefix = $this->conn->tablePrefix();
  }

  /**
   * Validates this command, and sets error(s) if the command is incorrect
   */
  public function validate() {
    //Check that the database is a mysql database
    $info = reset(Database::getConnectionInfo());
    if ($info['driver'] != 'mysql') {
      return drush_set_error('NO_MYSQL_DATABASE', dt('This command is only supported for mysql databases, but the database of this site is of type: !driver.', array('!driver' => $info['driver'])));
    }
    
    if (reset($info['prefix']) != '') {
      return drush_set_error('PREFIX_NOT_SUPPORTED', dt('This command is not supported for prefixed table database, but this site\'s database has a prefix: !prefix.', array('!prefix' => reset($info['prefix']))));
    }
    
    if ($this->search == NULL) {
      return drush_set_error('NO_SEARCH', dt('Please specify the search argument.'));
    }
    
    if ($this->replacement == NULL) {
      return drush_set_error('NO_REPLACEMENT', dt('Please specify the replacement argument.'));
    }
  }
  
  /**
   * Returns true iff the specified column type is replaceable
   */
  private function isReplaceableColumnType($type) {
    switch($type) {
      case "varchar":
      case "char":
      case "mediumtext": 
      case "longtext": 
      case "text": 
      case "longblob": 
      case "blob": 
      case "mediumblob":
        return true;
      default:
        return false;
    }
  }
  
  /**
   * Escapes a SQL like-value and place '%' to front and end
   */
  private function escapeLikeValue($value) {
    $value = str_replace("%", "\\%", $value);
    $value = str_replace("_", "\\_", $value);
    return "%" . $value . "%";
  }
  
  /**
   * Start replacing (this may be a slow function)
   */  
  public function execute() {
    //Get all tables
    $tables = $this->getTables();
    
    foreach($tables as $table) {
      //Add a temporary replacement index column, so that the records can be identified
      // (it will be removed later)
      try {
        $this->conn->query("ALTER TABLE {$table} DROP __replacement_index");
      } catch(Exception $e) {}
      $this->conn->query("ALTER TABLE {$table} ADD __replacement_index INT NULL");
      $this->conn->query("ALTER TABLE {$table} ADD INDEX ( __replacement_index )");
      $this->conn->query("SET @index := 0");
      $this->conn->query("UPDATE {$table} SET __replacement_index = (@index := @index + 1)");
    }
    
    //Start the DB transaction
    $this->conn->query("START TRANSACTION");

    try {
      foreach ($tables as $table) {
        //Get all columns
        $columns = $this->getColumns($table);
  
        //Replace values in all columns
        foreach ($columns as $column => $type) {
          if ($column == '__replacement_index') continue;
          
          if ($this->isReplaceableColumnType($type)) {
            //Check all serialized column values
            $regexp1 = '^a:[[:digit:]]+:{';
            $regexp2 = '^O:[[:digit:]]+:\"';
            $regexp3 = '^s:[[:digit:]]+:\"';
            $regexpPart = "($column REGEXP :regexp1) OR ({$column} REGEXP :regexp2) OR ({$column} REGEXP :regexp3)";
            $result = $this->conn->query("SELECT {$column} AS val, __replacement_index FROM {$table} WHERE ($regexpPart) AND ({$column} LIKE :search)",
              array(':search' => $this->escapeLikeValue($this->search),
                ':regexp1' => $regexp1,
                ':regexp2' => $regexp2,
                ':regexp3' => $regexp3));
            
            $rowCount = 0;    
            foreach($result as $record) {
              $val = $record->val;
              $obj = unserialize($val);
              if ($obj) {
                //Replace recursively
                drupix_replace_recursively($obj, $this->search, $this->replacement);
                $val = serialize($obj);
              } else {
                //String was not serialized after all: use a normal replace
                $val = str_replace($this->search, $this->replacement, $val);
              }
              
              //Update the record
              $this->conn->update($table)
                ->condition('__replacement_index', $record->__replacement_index)
                ->fields(array($column => $val, '__replacement_index' => NULL))
                ->execute();
              
              $rowCount++;
            }
            
            //Simple replace of non-serialized values
            $result = $this->conn->query("UPDATE {$table} SET {$column} = REPLACE({$column}, :search, :replacement) WHERE __replacement_index IS NOT NULL", array(':search' => $this->search, ':replacement' => $this->replacement));
            $rowCount += $result->rowCount;
            if ($rowCount > 0) {
              drush_print("Replaced $rowCount record in $table.$column");
            }
          }
        }
      }

      $this->conn->query("COMMIT");
    } catch(Exception $e) {
      $this->conn->query("ROLLBACK");
      return drush_set_error('GENERAL_ERROR', "" . $e->getMessage());
    }
    
    //Drop temporary replacement index columns
    foreach ($tables as $table) {
      try {
        $this->conn->query("ALTER TABLE {$table} DROP __replacement_index");
      } catch(Exception $e) {
         /* Ignore and try to alter other tables */ 
         drush_print("Warning: can't remove temporary column __replacement_index from table '$table'");
      }
    }
    
    drush_log("The replacement operation has completed.", 'success');
  }
  
  /**
   * Returns (the names of) all database tables to be searched
   * @return [String $name]
   */
  private function getTables() {
    //Get all tables to be searched through
    $result = db_query("SELECT table_name FROM {information_schema}.{TABLES} WHERE (:prefix = '' OR LOCATE(:prefix, table_name) = 1) AND table_schema = :database AND table_type = 'BASE TABLE'", array(':database' => $this->dbName, ':prefix' => $this->prefix));
    $tables = array();
    foreach($result as $record) {
      $tables[] = $record->table_name;
    }
    return $tables;
  }
  
  /**
   * Returns the names and types of all of the columns in the specified table
   * @return [String $name => String $type]
   */
  private function getColumns($table) {
    //Get all tables to be searched through
    $result = db_query("SELECT column_name, data_type FROM {information_schema}.{COLUMNS} WHERE table_schema = :database AND table_name = :table", array(':database' => $this->dbName, ':table' => $table));
    $columns = array();
    foreach($result as $record) {
      $columns[$record->column_name] = $record->data_type;
    }
    return $columns;
  }
  
}

/**
 * Recursively searches the object for occurrences of rep, and replaces those with by
 * @param Object $obj   The object in which to search
 * @param String $rep   The search string
 * @param String $by    The replacement string
 */
function drupix_replace_recursively(&$obj, $rep, $by) {
  if (is_array($obj)) {
    //Loop through all elements
    foreach ($obj as &$val) {
      drupix_replace_recursively($val, $rep, $by);
    }
  } else if (is_object($obj)) {
    $vars = get_object_vars($obj);
    foreach (array_keys($vars) as $prop) {
      drupix_replace_recursively($obj->$prop, $rep, $by);
    }
  } else if (is_string($obj)) {
    if (preg_match("/^(a:\d+:{|O:\d+:\"|s:\d+:\")/", $obj)) {
      $refObj = @unserialize($obj);
      if ($refObj === false) {
        //Handle as simple string after all
        $obj = str_replace($rep, $by, $obj);
      } else {
        //Replace array recursively
        drupix_replace_recursively($refObj, $rep, $by);
        
        //Set obj
        $obj = serialize($refObj);
      }
    } else {
      //Simple string
      $obj = str_replace($rep, $by, $obj);
    }
  }
}
?>